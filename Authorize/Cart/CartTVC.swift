//
//  CartViewController.swift
//  Authorize
//
//  Created by KD's on 8/7/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit

class CartTVC: UITableViewController {
    
    var total: Int = 0
    var cart: Categorylist?
    var info: UserInfo = UserInfo(Name: "", Email: "", ContactNumber: "")
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundView = UIImageView(image: UIImage(named: "img"))
        tableView.separatorColor = UIColor.clear
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Checkout" {
            let controller = segue.destination as! CheckoutVC
            controller.info = self.info
            controller.total = self.total
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (cart?.items.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartItem", for: indexPath) as! CartCell
        let item = cart?.items[indexPath.row]
        cell.delegate = self
        cell.indexPath = indexPath
        cell.price = item?.price
        cell.quantity = (item?.quantity)!
        cell.iconNameLabel.text = item?.text
        cell.iconImageView.image = UIImage(named: (item?.iconName)!)
        cell.cellImageView.layer.cornerRadius = 20
        cell.priceLabel.text = "Price: \(item!.price)"
        cell.totalLabel.text = "Total: \(item!.total)"
        cell.quantityLabel.text = String(cell.quantity)
        return cell
    }

    @IBAction func checkout(_ sender: Any) {
        for item in (cart?.items)! {
            total += item.total
        }
        performSegue(withIdentifier: "Checkout", sender: nil)
    }
    
}

extension CartTVC: CartCellDelegate {
    
    func didIncrease(index: Int, quantity: Int) {
        if let item = self.cart?.items[index] {
            let price = item.price
            item.total = price * quantity
            item.quantity = quantity
            self.cart?.items[index] = item
            let indexPath = IndexPath(row: index, section: 0)
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    func didDecrease(index: Int, quantity: Int) {
        if let item = self.cart?.items[index] {
            let price = item.price
            item.total = price * quantity
            item.quantity = quantity
            self.cart?.items[index] = item
            let indexPath = IndexPath(row: index, section: 0)
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
}
