//
//  CartCell.swift
//  Authorize
//
//  Created by KD's on 8/19/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation
import UIKit

protocol CartCellDelegate: class {
    func didIncrease(index: Int, quantity: Int)
    func didDecrease(index: Int, quantity: Int)
}

class CartCell: UITableViewCell {
    
    weak var delegate: CartCellDelegate?
    var indexPath: IndexPath?
    var quantity: Int = 1
    var price: Int?
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBAction func decrease(_ sender: Any) {
        if quantity > 0 {
            quantity -= 1
        delegate?.didDecrease(index: (indexPath?.row)!, quantity: quantity)
        }
    }
    @IBAction func increase(_ sender: Any) {
        quantity += 1
        delegate?.didIncrease(index: (indexPath?.row)!, quantity: quantity)
        
    }
    
}
