//
//  SignUpViewController.swift
//  Authorize
//
//  Created by KD's on 7/29/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit
import Firebase

class SignUpVC: UIViewController {
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var contactNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var btnSignupImageView: UIImageView!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnSignupImageView.layer.cornerRadius = 20
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userNameTextField.becomeFirstResponder()
       
    }
    
    @IBAction func signUp(_ sender: Any) {
        let emptyFields = areFieldsEmpty(ForUsername: userNameTextField.text!, ForEmail: emailTextField.text!, ForContactNumber: contactNumberTextField.text!, ForPassword: passwordTextField.text!, ForConfirmPassword: confirmPasswordTextField.text!)
        if emptyFields == false {
            return
        }
        if !isValidEmail(email: emailTextField.text!) {
            print("Invalid Email")
            return
        }
        if !isValidPassword(testStr: passwordTextField.text!) {
            print("Password must contain one uppercase, one lowercase, one number and must be of 8 characters")
            return
        }
        if passwordTextField.text! != confirmPasswordTextField.text! {
            print("Passwords do not match")
            return
        }
        checkUniqueUserName(ForUsername: userNameTextField.text!)
    }
    
    @IBAction func signIn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func isValidEmail(email:String?) -> Bool {
        guard email != nil else { return false }
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }
    
    func isValidPhone(number: String?) -> Bool {
        guard number != nil else {return false}
        let regularExpressionForPhone = "^\\d{3}-\\d{3}-\\d{4}$"
        let testPhone = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
        return testPhone.evaluate(with: number)
    }
    
    func areFieldsEmpty(ForUsername username: String, ForEmail email: String,ForContactNumber contactNumber: String, ForPassword password: String, ForConfirmPassword confirmPassword: String) -> Bool {
        if username.isEmpty {
            print("Enter Username")
            return false
        } else if email.isEmpty {
            print("Enter Email")
            return false
        } else if contactNumber.isEmpty {
            print("Enter Contact Number")
            return false
        } else if password.isEmpty {
            print("Enter Password")
            return false
        } else if confirmPassword.isEmpty {
            print("Enter Confirm Password")
            return false
        } else {
            return true
        }
    }
    
    func saveData(ForUsername username: String, ForEmail email: String,ForContactNumber contactNumber: String, ForPassword password: String) {
        var ref: DocumentReference? = nil
        ref = db.collection("Users").addDocument(data: ["UserName": username,
            "Email": email,
            "ContactNumber": "\(contactNumber)",
            "Password": password,
            "AccountType": "User"
        ]) {err in
                if let err = err {
                    print("Error adding documnet: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                    self.navigationController?.popToRootViewController(animated: true)
                }
        }
    }
    
    func checkUniqueUserName(ForUsername username: String) {
        db.collection("Users").whereField("UserName", isEqualTo: username)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else if (querySnapshot?.isEmpty)! == false  {
                    print("UserName already registered")
                    self.userNameTextField.text = ""
                    return
                } else {
                    self.checkUniqueEmail(ForEmail: self.emailTextField.text!)
                }
        }
    }
    
    func checkUniqueEmail(ForEmail email: String) {
        db.collection("Users").whereField("Email", isEqualTo: email)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else if (querySnapshot?.isEmpty)! == false  {
                    print("Email already registered")
                    self.emailTextField.text = ""
                    return
                } else {
                    self.checkUniqueContactNumber(ForContactNumber: self.contactNumberTextField.text!)
                }
        }
    }
    
    func checkUniqueContactNumber(ForContactNumber contactNumber: String) {
        db.collection("Users").whereField("ContactNumber", isEqualTo: "\(contactNumber)")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else if (querySnapshot?.isEmpty)! == false  {
                    print("Contact Number already registered")
                    self.contactNumberTextField.text = ""
                    return
                } else {
                    self.saveData(ForUsername: self.userNameTextField.text!, ForEmail: self.emailTextField.text!, ForContactNumber: self.contactNumberTextField.text!, ForPassword: self.passwordTextField.text!)
                }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
}
