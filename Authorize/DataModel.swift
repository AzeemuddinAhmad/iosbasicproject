//
//  DataModel.swift
//  Authorize
//
//  Created by KD's on 8/9/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation

class DataModel {
    var lists = [Categorylist]()
    
    init() {
        loadCategorylists()
    }
    
    
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent("CategoryItems.plist")
    }
    
    func saveCategorylists() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(lists, forKey: "Categorylists")
        archiver.finishEncoding()
        data.write(to: dataFilePath(), atomically: true)
    }
    
    func loadCategorylists() {
        let path = dataFilePath()
        
        if let data = try? Data(contentsOf: path) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            lists = unarchiver.decodeObject(forKey: "Categorylists") as! [Categorylist]
        }
    }
    
    
}
