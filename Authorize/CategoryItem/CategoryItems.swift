//
//  CategoryItem.swift
//  Authorize
//
//  Created by KD's on 8/5/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation

class CategoryItems: NSObject, NSCoding {
    var text = ""
    var iconName = ""
    var price = 0
    var total = 0
    var quantity = 0
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        text = aDecoder.decodeObject(forKey: "Text") as! String
        price = aDecoder.decodeInteger(forKey: "Price")
        iconName = aDecoder.decodeObject(forKey: "IconName") as! String
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(text, forKey: "Text")
        aCoder.encode(price, forKey: "Price")
        aCoder.encode(iconName, forKey: "IconName")
    }
}
