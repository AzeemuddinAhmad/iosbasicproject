//
//  AdminViewController.swift
//  Authorize
//
//  Created by KD's on 8/4/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation
import UIKit

class AdminCategoryTVC: UITableViewController {

    var categorylist: Categorylist!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = categorylist.name
        tableView.separatorColor = UIColor.clear
        tableView.backgroundView = UIImageView(image: UIImage(named: "img")) 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddItem" {
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! AddOrEditCategoryItemsVC
            controller.delegate = self
        } 
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categorylist.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryItem" , for: indexPath) as! AdminCategoryitemCell
        let item = categorylist.items[indexPath.row]
        cell.delegate = self
        cell.indexPath = indexPath
        cell.cellImage.layer.cornerRadius = 20
        cell.iconLabel.text = item.text
        cell.priceLabel.text = "Price: \(item.price)"
        cell.iconImage.image = UIImage(named: item.iconName)
        return cell
    }
   
}

extension AdminCategoryTVC: AdminCategoryitemCellDelegate {
    
    func didEdit(indexPath: IndexPath) {
        let navigationController = storyboard!.instantiateViewController(withIdentifier: "ItemDetailNavigationController") as! UINavigationController
        let controller = navigationController.topViewController as! AddOrEditCategoryItemsVC
        controller.delegate = self
        let item = categorylist.items[indexPath.row]
        controller.itemToEdit = item
        present(navigationController, animated: true, completion: nil)
    }
    
    func didDelete(indexPath: IndexPath) {
        categorylist.items.remove(at: indexPath.row)
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
        
    }
    
}

extension AdminCategoryTVC: AddOrEditCategoryItemsViewControllerDelegate {
    
    func addOrEditCategoryItemsViewControllerDidCancel(_ controller: AddOrEditCategoryItemsVC) {
        dismiss(animated: true, completion: nil)
    }
    
    func addOrEditCategoryItemsViewController(_ controller: AddOrEditCategoryItemsVC, didFinishAdding item: CategoryItems) {
        let newRowIndex = categorylist.items.count
        categorylist.items.append(item)
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic)
        dismiss(animated: true, completion: nil)
    }
    
    func addOrEditCategoryItemsViewController(_ controller: AddOrEditCategoryItemsVC, didFinishEditing item: CategoryItems) {
        if let index = categorylist.items.firstIndex(where: {$0 === item}) {
            let indexPath = IndexPath(row: index, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as! AdminCategoryitemCell
            cell.iconLabel.text = item.text
            cell.priceLabel.text = "Price: \(item.price)"
            cell.iconImage.image = UIImage(named: item.iconName)
        }
        dismiss(animated: true, completion: nil)
    }
    
}
