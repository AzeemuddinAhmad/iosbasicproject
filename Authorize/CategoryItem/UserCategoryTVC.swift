//
//  UserViewController.swift
//  Authorize
//
//  Created by KD's on 8/4/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//
import Foundation
import UIKit

protocol UserCategoryViewControllerDelegate: class {
    func userCategoryvewCOntrollerDelegate(_ controller: UserCategoryTVC, didFinish cart: Categorylist)
}

class UserCategoryTVC: UITableViewController {
    var categorylist: Categorylist!
    var cart: Categorylist!
    weak var delegate: UserCategoryViewControllerDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        cart = Categorylist(name: "Cart")
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = categorylist.name
        tableView.separatorColor = UIColor.clear
        tableView.backgroundView = UIImageView(image: UIImage(named: "img"))
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categorylist.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell" , for: indexPath) as! UserCategoryitemCell
        let item = categorylist.items[indexPath.row]
        cell.cellImage.layer.cornerRadius = 20
        cell.iconLabel.text = item.text
        cell.priceLabel.text = "Price: \(item.price)"
        cell.iconImage.image = UIImage(named: item.iconName)
        cell.cellAccessoryType = false
        cell.selectedBackgroundView = UIView(frame: CGRect.zero)
        cell.selectedBackgroundView?.backgroundColor = UIColor(red: 0.27, green: 0.71, blue: 0.73, alpha: 0.0)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! UserCategoryitemCell
        let item = categorylist.items[indexPath.row]
        confirgueCheckmark(for: cell, with: item)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func done() {
        delegate?.userCategoryvewCOntrollerDelegate(self, didFinish: cart)
    }
    
    func confirgueCheckmark(for cell: UserCategoryitemCell, with item: CategoryItems) {
        if  cell.cellAccessoryType == false {
            cell.cellAccessoryType = true
            cell.checkmarkImage.image = UIImage(named: "Checkmark")
            item.total = item.price
            item.quantity = 1
            cart.items.append(item)
        } else if cell.cellAccessoryType == true {
            cell.cellAccessoryType = false
            cell.checkmarkImage.image = nil
            if let index = cart.items.firstIndex(where: {$0 === item}) {
                cart.items.remove(at: index)
            }
        }
    }
}
