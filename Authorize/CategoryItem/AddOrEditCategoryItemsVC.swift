//
//  AddCategory.swift
//  Authorize
//
//  Created by KD's on 8/4/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation
import UIKit

protocol AddOrEditCategoryItemsViewControllerDelegate: class {
    func addOrEditCategoryItemsViewControllerDidCancel(_ controller: AddOrEditCategoryItemsVC)
    func addOrEditCategoryItemsViewController(_ controller: AddOrEditCategoryItemsVC, didFinishAdding item: CategoryItems)
    func addOrEditCategoryItemsViewController(_ controller: AddOrEditCategoryItemsVC, didFinishEditing item: CategoryItems)
}

class AddOrEditCategoryItemsVC: UITableViewController, UITextFieldDelegate {
    
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconLabel: UILabel!
    
    weak var delegate: AddOrEditCategoryItemsViewControllerDelegate?
    
    var itemToEdit: CategoryItems?
    var iconName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = itemToEdit {
            title = "Edit Category"
            nameTextField.text = item.text
            priceTextField.text = String(item.price)
            doneBarButton.isEnabled = true
            iconName = item.iconName
            iconLabel.text = item.iconName
            iconImageView.image = UIImage(named: iconName)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.backgroundView = UIImageView(image: UIImage(named: "img"))
        tableView.backgroundColor = UIColor.darkText
        tableView.separatorColor = UIColor.clear
        nameTextField.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PickIcon" {
            let controller = segue.destination as! CategoryItemIconsVC
            controller.delegate = self
        }
    }
    
    @IBAction func done() {
        if let item = itemToEdit {
            item.text = nameTextField.text!
            item.price = Int(priceTextField.text!)!
            item.iconName = iconName
            delegate?.addOrEditCategoryItemsViewController(self, didFinishEditing: item)
        } else {
            let item = CategoryItems()
            item.text = nameTextField.text!
            item.price = Int(priceTextField.text!)!
            item.iconName = iconName
            delegate?.addOrEditCategoryItemsViewController(self, didFinishAdding: item)
        }
    }
    @IBAction func cancel() {
        delegate?.addOrEditCategoryItemsViewControllerDidCancel(self)
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 {
            return indexPath
        } else {
            return nil
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string) as NSString
        doneBarButton.isEnabled = newText.length > 0
        return true
    }
    
}

extension AddOrEditCategoryItemsVC: CategoryItemIconsViewcontrollerDelegate {
    func categoryitemIconPicker(_ picker: CategoryItemIconsVC, didPick iconName: String) {
        self.iconName = iconName
        self.iconImageView.image = UIImage(named: iconName)
        self.iconLabel.text = iconName
        navigationController?.popViewController(animated: true)
    }
}
