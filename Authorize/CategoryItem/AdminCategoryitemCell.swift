//
//  AdminCategoryitemCell.swift
//  Authorize
//
//  Created by KD's on 8/20/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit

protocol AdminCategoryitemCellDelegate: class {
    func didEdit(indexPath: IndexPath)
    func didDelete(indexPath: IndexPath)
}

class AdminCategoryitemCell: UITableViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    weak var delegate: AdminCategoryitemCellDelegate?
    var indexPath: IndexPath?
    
    @IBAction func edit(_ sender: Any) {
        delegate?.didEdit(indexPath: indexPath!)
    }
    
    @IBAction func deleteItam(_ sender: Any) {
        delegate?.didDelete(indexPath: indexPath!)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
