//
//  Cart.swift
//  Authorize
//
//  Created by KD's on 8/7/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation

class UserInfo {
    var name: String
    var email: String
    var contactNumber: String

    init(Name name: String, Email email: String, ContactNumber number: String) {
        self.name = name
        self.email = email
        self.contactNumber = number
    }
    
}
