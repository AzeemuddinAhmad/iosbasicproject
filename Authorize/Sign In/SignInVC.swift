//
//  ViewController.swift
//  Authorize
//
//  Created by KD's on 7/22/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit
import Firebase

class SignInVC: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInFormImageView: UIImageView!
    var name: String = ""
    var email: String = ""
    var contactNumber: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.logInFormImageView.layer.cornerRadius = 20
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        usernameTextField.becomeFirstResponder()
    }
    
    @IBAction func signIn(_ sender: Any) {
        checkUsername(ForUsername: usernameTextField.text!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "User" {
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! UserAllCategoriesTVC
            controller.info.name = self.name
            controller.info.email = self.email
            controller.info.contactNumber = self.contactNumber
        }
    }
    
    func checkUsername(ForUsername username: String) {
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        db.collection("Users").whereField("UserName", isEqualTo: self.usernameTextField.text!)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else if (querySnapshot?.isEmpty)! == true  {
                    self.showToast(controller: self, message: "User Name not registered", seconds: 2)
                    self.usernameTextField.text = ""
                    return
                } else {
                    for document in querySnapshot!.documents {
                        let password: String = document.get("Password") as! String
                        if password == self.passwordTextField.text {
                            self.usernameTextField.text = ""
                            self.passwordTextField.text = ""
                            if document.get("AccountType") as! String == "User" {
                                self.name = document.get("UserName") as! String
                                self.email = document.get("Email") as! String
                                self.contactNumber = document.get("ContactNumber") as! String
                                self.performSegue(withIdentifier: "User", sender: nil)
                            } else if document.get("AccountType") as! String == "Admin" {
                                self.performSegue(withIdentifier: "Admin", sender: nil)
                            }
                        } else {
                            self.showToast(controller: self, message: "Incorrect password", seconds: 2)
                            self.passwordTextField.text = ""
                        }
                    }
                    
                }
        }
    }
    
    func showToast(controller: UIViewController, message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = .black
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 20
        
        present(alert , animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
}

