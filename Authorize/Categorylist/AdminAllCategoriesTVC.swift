//
//  AdminCategorylistViewController.swift
//  Authorize
//
//  Created by KD's on 8/6/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit

class AdminAllCategoriesTVC: UITableViewController {
    
    var lists: [Categorylist]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorColor = UIColor.clear
        tableView.backgroundView = UIImageView(image: UIImage(named: "img")) 
        print(documentsDirectory())
    }
    
    required init?(coder aDecoder: NSCoder) {
        lists = [Categorylist]()
        super.init(coder: aDecoder)
        print("\(documentsDirectory())")
        loadCategorylists()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AdminCategoryItemslist" {
            let controller = segue.destination as! AdminCategoryTVC
            controller.categorylist = (sender as! Categorylist)
        } else if segue.identifier == "AddCategory" {
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! AddOrEditCategorylistVC
            controller.delegate = self
            controller.categorylistToEdit = nil
        }
        
    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AdminCategorylistCell
        let categorylist = lists[indexPath.row]
        cell.delegate = self
        cell.indexPath = indexPath
        cell.cellImage.layer.cornerRadius = 20
        cell.iconLabel.text = categorylist.name
        cell.iconImage.image = UIImage(named: categorylist.iconName)
        cell.selectedBackgroundView = UIView(frame: CGRect.zero)
        cell.selectedBackgroundView?.backgroundColor = UIColor(red: 0.27, green: 0.71, blue: 0.73, alpha: 0.0)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categorylist = lists[indexPath.row]
        performSegue(withIdentifier: "AdminCategoryItemslist", sender: categorylist)
    }
    
    @IBAction func signOut(_ sender: Any) {
        self.saveCategorylists() 
        let alertController = UIAlertController(title: "Sign Out?", message: "Do you want to sign out?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Sign Out", style: .default, handler: {action in self.dismiss(animated: true, completion: nil)})
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(action)
        alertController.addAction(cancel)
        present(alertController, animated: true)
    }
    
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent("Categorylists.plist")
    }
    
    func saveCategorylists() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(lists, forKey: "Categorylists")
        archiver.finishEncoding()
        data.write(to: dataFilePath(), atomically: true)
    }
    
    func loadCategorylists() {
        let path = dataFilePath()
        
        if let data = try? Data(contentsOf: path) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            lists = unarchiver.decodeObject(forKey: "Categorylists") as! [Categorylist]
        }
    }
    
}

extension AdminAllCategoriesTVC: AdminCategorylistCellDelegate {
    
    func didDelete(indexPath: IndexPath) {
        self.lists.remove(at: indexPath.row)
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    func didEdit(index: Int) {
        let navigationController = storyboard!.instantiateViewController(withIdentifier: "CategoryDetailNavigationController") as! UINavigationController
        let controller = navigationController.topViewController as! AddOrEditCategorylistVC
        controller.delegate = self
        let indexPath = IndexPath(row: index, section: 0)
        let categorylist = lists[indexPath.row]
        controller.categorylistToEdit = categorylist
        present(navigationController, animated: true, completion: nil)
    }
    
}

extension AdminAllCategoriesTVC: AddOrEditCategoryViewControllerDelegate {
    
    func addOrEditCategoryViewControllerDidCancel(_ controller: AddOrEditCategorylistVC) {
        dismiss(animated: true, completion: nil)
    }
    
    func addOrEditCategoryViewController(_ controller: AddOrEditCategorylistVC, didFinishAdding categorylist: Categorylist) {
        let newRowIndex = lists.count
        lists.append(categorylist)
        
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic)
        dismiss(animated: true, completion: nil)
        saveCategorylists()
    }
    
    func addOrEditCategoryViewController(_ controller: AddOrEditCategorylistVC, didFinishEditing categorylist: Categorylist) {
        if let index = lists.firstIndex(where: {$0 === categorylist}) {
            let indexPath = IndexPath(row: index, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as! AdminCategorylistCell
            cell.iconLabel.text = categorylist.name
            cell.iconImage.image = UIImage(named: categorylist.iconName)
        }
        dismiss(animated: true, completion: nil)
        saveCategorylists()
    }
    
}
