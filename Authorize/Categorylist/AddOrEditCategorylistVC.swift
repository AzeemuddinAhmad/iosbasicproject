//
//  AddOrEditCategoryViewController.swift
//  Authorize
//
//  Created by KD's on 8/6/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit

protocol AddOrEditCategoryViewControllerDelegate: class {
    func addOrEditCategoryViewControllerDidCancel(_ controller: AddOrEditCategorylistVC)
    func addOrEditCategoryViewController(_ controller: AddOrEditCategorylistVC, didFinishAdding categorylist: Categorylist)
    func addOrEditCategoryViewController(_ controller: AddOrEditCategorylistVC, didFinishEditing categorylist: Categorylist)
}

class AddOrEditCategorylistVC: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconLabel: UILabel!
    
    weak var delegate: AddOrEditCategoryViewControllerDelegate?
    
    var categorylistToEdit: Categorylist?
    var iconName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let categorylist = categorylistToEdit {
            title = "Edit Category"
            nameTextField.text = categorylist.name
            doneBarButton.isEnabled = true
            iconName = categorylist.iconName
            iconLabel.text = categorylist.iconName
            iconImageView.image = UIImage(named: iconName)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.backgroundView = UIImageView(image: UIImage(named: "img"))
        tableView.backgroundColor = UIColor.darkText
        tableView.separatorColor = UIColor.clear
        nameTextField.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PickIcon" {
            let controller = segue.destination as! CategorylistIconsVC
            controller.delegate = self
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 {
            return indexPath
        } else {
            return nil
        }
    }
    
    @IBAction func done() {
        if let categorylist = categorylistToEdit {
            categorylist.name = nameTextField.text!
            categorylist.iconName = iconName
            delegate?.addOrEditCategoryViewController(self, didFinishEditing: categorylist)
        } else {
            let categorylist = Categorylist(name: nameTextField.text!, iconName: iconName)
            delegate?.addOrEditCategoryViewController(self, didFinishAdding: categorylist)
        }
    }
    
    @IBAction func cancel() {
        delegate?.addOrEditCategoryViewControllerDidCancel(self)
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string) as NSString
        doneBarButton.isEnabled = newText.length > 0
        return true
    }
  
}

extension AddOrEditCategorylistVC: CategorylistIconsViewControllerDelegate {
    
    func categorylistIconPicker(_ picker: CategorylistIconsVC, didPick iconName: String) {
        self.iconName = iconName
        self.iconImageView.image = UIImage(named: iconName)
        self.iconLabel.text = iconName
        navigationController?.popViewController(animated: true)
    }
    
}
