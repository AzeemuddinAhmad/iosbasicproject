//
//  UserCategorylistCell.swift
//  Authorize
//
//  Created by KD's on 8/20/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit

class UserCategorylistCell: UITableViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var iconLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
