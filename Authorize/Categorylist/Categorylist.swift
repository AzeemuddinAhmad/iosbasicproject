//
//  Categorylist.swift
//  Authorize
//
//  Created by KD's on 8/6/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation

class Categorylist: NSObject, NSCoding {
    var name = ""
    var iconName: String
    var items = [CategoryItems]()
    
    convenience init(name: String) {
        self.init(name: name, iconName: "No Icon")
    }
    
    init(name: String, iconName: String) {
        self.name = name
        self.iconName = iconName
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "Name") as! String
        items = aDecoder.decodeObject(forKey: "Items") as! [CategoryItems]
        iconName = aDecoder.decodeObject(forKey: "IconName") as! String
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "Name")
        aCoder.encode(items, forKey: "Items")
        aCoder.encode(iconName, forKey: "IconName")
    }
}
