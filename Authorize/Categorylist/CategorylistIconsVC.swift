//
//  CateogrylistIconsViewController.swift
//  Authorize
//
//  Created by KD's on 8/19/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import Foundation
import UIKit

protocol CategorylistIconsViewControllerDelegate: class {
    func categorylistIconPicker(_ picker: CategorylistIconsVC, didPick iconName: String)
}

class CategorylistIconsVC: UITableViewController {
    weak var delegate: CategorylistIconsViewControllerDelegate?
    
    var icons = ["No Icon", "Birthday", "Clothes", "Cutlery", "Decorations", "Footwear", "Groceries", "Jewelry", "Kitchenware", "Stationary"]
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return icons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IconCell", for: indexPath)
        let iconName = icons[indexPath.row]
        cell.textLabel!.text = iconName
        cell.imageView?.image = UIImage(named: iconName)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = delegate {
            let iconName = icons[indexPath.row]
            delegate.categorylistIconPicker(self, didPick: iconName)
        }
    }
}
