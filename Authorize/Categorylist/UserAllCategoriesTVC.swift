//
//  UserCategorylistViewController.swift
//  Authorize
//
//  Created by KD's on 8/6/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit

class UserAllCategoriesTVC: UITableViewController {
    
    var cart: Categorylist!
    var lists: [Categorylist]
    var info: UserInfo = UserInfo(Name: "", Email: "", ContactNumber: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorColor = UIColor.clear
        tableView.backgroundView = UIImageView(image: UIImage(named: "img")) 
    }
    
    required init?(coder aDecoder: NSCoder) {
        lists = [Categorylist]()
        cart = Categorylist(name: "Cart")
        super.init(coder: aDecoder)
        loadCategorylists()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UserCategoryItemslist" {
            let controller = segue.destination as! UserCategoryTVC
            controller.categorylist = (sender as! Categorylist)
            controller.delegate = self
        } else if segue.identifier == "ProceedToCart" {
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! CartTVC
            controller.cart = cart
            controller.info = self.info
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserCategorylistCell
        let categorylist = lists[indexPath.row]
        cell.cellImage.layer.cornerRadius = 20
        cell.iconLabel.text = categorylist.name
        cell.iconImage.image = UIImage(named: categorylist.iconName)
        cell.selectedBackgroundView = UIView(frame: CGRect.zero)
        cell.selectedBackgroundView?.backgroundColor = UIColor(red: 0.27, green: 0.71, blue: 0.73, alpha: 0.0)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categorylist = lists[indexPath.row]
        performSegue(withIdentifier: "UserCategoryItemslist", sender: categorylist)
    }
    
    @IBAction func done() {	
        if self.cart.items.count != 0 {
                performSegue(withIdentifier: "ProceedToCart", sender: self.cart)
        } else {
            print("Cart is empty")
        }

    }
    
    @IBAction func signOut() {
        let alertController = UIAlertController(title: "Sign Out?", message: "Do you want to sign out?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Sign Out", style: .default, handler: {action in self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)})
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(action)
        alertController.addAction(cancel)
        present(alertController, animated: true)
    }
    
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent("Categorylists.plist")
    }
    
    func loadCategorylists() {
        let path = dataFilePath()
        
        if let data = try? Data(contentsOf: path) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            lists = unarchiver.decodeObject(forKey: "Categorylists") as! [Categorylist]
        }
    }
}

extension UserAllCategoriesTVC: UserCategoryViewControllerDelegate {
    
    func userCategoryvewCOntrollerDelegate(_ controller: UserCategoryTVC, didFinish cart: Categorylist) {
        for items in cart.items {
            if !self.cart.items.contains(items) {
                self.cart.items.append(items)
            }
        }
        navigationController?.popViewController(animated: true)
    }
    
}
