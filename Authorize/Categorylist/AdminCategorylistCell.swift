//
//  AdminCategorylistCell.swift
//  Authorize
//
//  Created by KD's on 8/20/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit

protocol AdminCategorylistCellDelegate: class {
    func didEdit(index: Int)
    func didDelete(indexPath: IndexPath)
}

class AdminCategorylistCell: UITableViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var iconLabel: UILabel!
    
    weak var delegate: AdminCategorylistCellDelegate?
    var indexPath: IndexPath?
    
    @IBAction func edit(_ sender: Any) {
        delegate?.didEdit(index: (indexPath?.row)!)
    }
    
    @IBAction func deletelist(_ sender: Any) {
        delegate?.didDelete(indexPath: indexPath!)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
