//
//  CheckoutViewController.swift
//  Authorize
//
//  Created by KD's on 8/20/1398 AP.
//  Copyright © 1398 KD. All rights reserved.
//

import UIKit
import MessageUI

class CheckoutVC: UIViewController {

    var info: UserInfo = UserInfo(Name: "", Email: "", ContactNumber: "")
    var total = 0
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var returnAmountLabel: UILabel!
    @IBOutlet weak var checkoutButtonImage: UIImageView!
    
    @IBAction func amountPaid(_ sender: Any) {
        if let amount = Int(self.amountTextField.text!) {
            let difference = amount - Int(totalLabel.text!)!
            self.returnAmountLabel.text = String(difference)
            checkoutButton.isEnabled = difference >= 0
        }
        
    }

    @IBAction func checkout(_ sender: Any) {
        let alertController = UIAlertController(title: "Shop More?", message: "", preferredStyle: .alert)
        let backToShop = UIAlertAction(title: "Go To Shop", style: .default, handler: {action in self.performSegue(withIdentifier: "ShopAgain", sender: self)})
        let signOut = UIAlertAction(title: "Sign Out", style: .default, handler: {action in self.performSegue(withIdentifier: "SignOut", sender: self)})
        alertController.addAction(backToShop)
        alertController.addAction(signOut)
        present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShopAgain" {
        let navigationController = segue.destination as! UINavigationController
        let controller = navigationController.topViewController as! UserAllCategoriesTVC
            controller.info = self.info
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameLabel.text = info.name
        emailLabel.text = info.email
        contactNumberLabel.text = info.contactNumber
        checkoutButtonImage.layer.cornerRadius = 20
        totalLabel.text = String(self.total)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        amountTextField.becomeFirstResponder()
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
}
